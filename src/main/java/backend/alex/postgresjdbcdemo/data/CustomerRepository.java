package backend.alex.postgresjdbcdemo.data;
import backend.alex.postgresjdbcdemo.models.domain.Customer;
import backend.alex.postgresjdbcdemo.models.dto.CountryCountDTO;
import backend.alex.postgresjdbcdemo.models.dto.CustomerSpentDTO;
import backend.alex.postgresjdbcdemo.models.dto.FavouriteGenreDTO;
import backend.alex.postgresjdbcdemo.models.mappers.CustomerSpentMapper;
import backend.alex.postgresjdbcdemo.models.mappers.CountryCountMapper;
import backend.alex.postgresjdbcdemo.models.mappers.FavouriteGenresMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerRepository {

    @Autowired
    CountryCountMapper countryCountMapper;

    @Autowired
    CustomerSpentMapper customerSpentMapper;

    @Autowired
    FavouriteGenresMapper favouriteGenresMapper;

    @Value("${spring.datasource.url}")
    private String url;

    //Get All Customers with specific data.
    public List<Customer> getAllCustomers() {
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email FROM customer as c";
        List<Customer> customers = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    customers.add((new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"), rs.getString("last_name"),
                            rs.getString("country"), rs.getString("postal_code"),
                            rs.getString("phone"), rs.getString("email"))));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    //Specific Get Customer.
    public Customer getSpecificCustomer(int id) {
        Customer customer = null;
        try {
            Connection con = DriverManager.getConnection(url);

            PreparedStatement preparedStatement = con.prepareStatement("SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email FROM customer as c WHERE c.customer_id = ?");
            preparedStatement.setInt(1, id);

            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    customer = new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public List<Customer> getCustomerbyName(String firstName) {
        List<Customer> customers = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(url);

            PreparedStatement preparedStatement = con.prepareStatement("SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email FROM customer AS c WHERE first_name = ?");
            preparedStatement.setString(1, firstName);
//            preparedStatement.setString(2, lastName);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                customers.add(new Customer(rs.getInt("customer_id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("country"),
                        rs.getString("postal_code"),
                        rs.getString("phone"),
                        rs.getString("email")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }


    public List<Customer> getCustomerByLimit(int limit, int offset) {
        List<Customer> customers = new ArrayList<>();
        try {
            Connection con = DriverManager.getConnection(url);

            PreparedStatement preparedStatement = con.prepareStatement("SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email FROM customer as c ORDER BY c.customer_id LIMIT ? OFFSET ?");
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);

            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    customers.add(new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public boolean addCustomer(Customer customer) {
        boolean success = false;
        try {
            Connection con = DriverManager.getConnection(url);

            PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }


    public boolean updateCustomer(int id, Customer customer) {
        boolean success = false;
        try {
            Connection con = DriverManager.getConnection(url);

            PreparedStatement preparedStatement = con.prepareStatement("UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, id);

            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }


    public CountryCountDTO getCountryCount() {
        return countryCountMapper.mapCountryPerCustomer();
    }

    public CustomerSpentDTO getCustomerSpent() {
        return customerSpentMapper.mapCustomerSpent();
    }

    public FavouriteGenreDTO getCustomerFavouriteGenre(int id) {
        return favouriteGenresMapper.mapFavouriteGenre(id);
    }

}
