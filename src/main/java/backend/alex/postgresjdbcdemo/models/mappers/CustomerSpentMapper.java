package backend.alex.postgresjdbcdemo.models.mappers;
import backend.alex.postgresjdbcdemo.models.dto.CustomerSpentDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class CustomerSpentMapper {

    @Value("${spring.datasource.url}")
    private String url;


    public CustomerSpentDTO mapCustomerSpent() {
        Map<String, BigDecimal> spent = new LinkedHashMap<>();
        try {
            System.out.println(url);
            Connection con = DriverManager.getConnection(url);

            PreparedStatement preparedStatement = con.prepareStatement("SELECT first_name || ' ' || last_name as full_name, SUM(inv.total) FROM customer as cust INNER JOIN invoice as inv ON inv.customer_id = cust.customer_id GROUP BY cust.customer_id ORDER BY SUM DESC");

            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    spent.put(
                            rs.getString("full_name"),
                            rs.getBigDecimal("sum"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new CustomerSpentDTO(spent);

    }
}
