package backend.alex.postgresjdbcdemo.models.mappers;
import backend.alex.postgresjdbcdemo.models.dto.CountryCountDTO;
import backend.alex.postgresjdbcdemo.models.dto.FavouriteGenreDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class FavouriteGenresMapper {

    @Value("${spring.datasource.url}")
    private String url;


    public FavouriteGenreDTO mapFavouriteGenre(int id) {
        Map<String, Integer> favouriteGenre = new LinkedHashMap<>();
        try {
            System.out.println(url);
            Connection con = DriverManager.getConnection(url);

            PreparedStatement preparedStatement = con.prepareStatement("SELECT g.\"name\" as genre_name, COUNT (il.invoice_line_id) as genre_total FROM genre as g INNER JOIN track as t ON t.genre_id = g.genre_id INNER JOIN invoice_line as il ON il.track_id = t.track_id INNER JOIN invoice as inv ON inv.invoice_id = il.invoice_id INNER JOIN customer as c ON c.customer_id = inv.customer_id AND c.customer_id = ? GROUP BY g.genre_id, g.\"name\" ORDER BY genre_total DESC FETCH FIRST 1 ROWS with TIES");
            preparedStatement.setInt(1, id);

            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    favouriteGenre.put(
                            rs.getString("genre_name"),
                            rs.getInt("genre_total"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new FavouriteGenreDTO(favouriteGenre);

    }
}
