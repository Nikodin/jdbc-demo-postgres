package backend.alex.postgresjdbcdemo.models.mappers;
import backend.alex.postgresjdbcdemo.models.dto.CountryCountDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class CountryCountMapper  {

    @Value("${spring.datasource.url}")
    private String url;


    public CountryCountDTO mapCountryPerCustomer() {
        Map<String, Integer> countryPerCustomers = new LinkedHashMap<>();
        try {
            System.out.println(url);
            Connection con = DriverManager.getConnection(url);

            PreparedStatement preparedStatement = con.prepareStatement("SELECT c.country, COUNT(*) FROM customer as c GROUP BY c.country ORDER BY COUNT(*) DESC");

            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    countryPerCustomers.put(
                            rs.getString("country"),
                            rs.getInt("count"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new CountryCountDTO(countryPerCustomers);

    }
}
