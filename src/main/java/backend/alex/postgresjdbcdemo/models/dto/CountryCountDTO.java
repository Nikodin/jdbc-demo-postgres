package backend.alex.postgresjdbcdemo.models.dto;
import java.util.Map;

public class CountryCountDTO {
    private Map<String, Integer> countryCount;

    public CountryCountDTO(Map<String, Integer> countryCount) {
        this.countryCount = countryCount;
    }

    public Map<String, Integer> getCountryCount() {
        return countryCount;
    }

    public void setCountryCount(Map<String, Integer> countryCount) {
        this.countryCount = countryCount;
    }
}
