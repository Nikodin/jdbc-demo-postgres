package backend.alex.postgresjdbcdemo.models.dto;
import java.util.Map;

public class FavouriteGenreDTO {
    private Map<String, Integer> favouriteGenre;

    public FavouriteGenreDTO(Map<String, Integer> countryCount) {
        this.favouriteGenre = countryCount;
    }

    public Map<String, Integer> getFavouriteGenre() {
        return favouriteGenre;
    }

    public void setFavouriteGenre(Map<String, Integer> favouriteGenre) {
        this.favouriteGenre = favouriteGenre;
    }
}
