package backend.alex.postgresjdbcdemo.models.dto;
import java.math.BigDecimal;
import java.util.Map;

public class CustomerSpentDTO {
    private Map<String, BigDecimal> consumerSpent;

    public CustomerSpentDTO(Map<String, BigDecimal> countryCount) {
        this.consumerSpent = countryCount;
    }

    public Map<String, BigDecimal> getConsumerSpent() {
        return consumerSpent;
    }

    public void setConsumerSpent(Map<String, BigDecimal> consumerSpent) {
        this.consumerSpent = consumerSpent;
    }
}
