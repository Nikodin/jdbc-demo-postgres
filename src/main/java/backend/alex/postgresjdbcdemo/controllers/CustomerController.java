package backend.alex.postgresjdbcdemo.controllers;

import backend.alex.postgresjdbcdemo.models.domain.Customer;
import backend.alex.postgresjdbcdemo.models.dto.CountryCountDTO;
import backend.alex.postgresjdbcdemo.models.dto.CustomerSpentDTO;
import backend.alex.postgresjdbcdemo.data.CustomerRepository;
import backend.alex.postgresjdbcdemo.models.dto.FavouriteGenreDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    private CustomerRepository customerRepository;


    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @GetMapping("/customers/id/{id}")
    public Customer getSpecificCustomer(@PathVariable int id) {return customerRepository.getSpecificCustomer(id);}

    @GetMapping("/customers/name/{firstName}")
    public List<Customer> getCustomerByName(@PathVariable String firstName) {return customerRepository.getCustomerbyName(firstName);}

    @GetMapping("/customers/{limit}/{offset}")
    public List<Customer> getCustomersByLimit(@PathVariable int limit, @PathVariable int offset) {return customerRepository.getCustomerByLimit(limit, offset);}

    @RequestMapping(value = "/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }


    @RequestMapping(value = "/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateCustomer(@PathVariable int id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(id, customer);
    }

    @GetMapping("/customers/countrycount")
    public ResponseEntity<CountryCountDTO> countryCount() {return new ResponseEntity<>(customerRepository.getCountryCount(), HttpStatus.OK);}

    @GetMapping("/customers/spent")
    public ResponseEntity<CustomerSpentDTO> spentCount() {return new ResponseEntity<>(customerRepository.getCustomerSpent(), HttpStatus.OK);}

    @GetMapping("/customers/{id}/favourites/genres")
    public ResponseEntity<FavouriteGenreDTO> favouriteGenres(@PathVariable int id) {return new ResponseEntity<>(customerRepository.getCustomerFavouriteGenre(id), HttpStatus.OK);}

}
